#setup.py
from distutils.core import setup
from distutils.extension import Extension
import os.path
import sys

# boost headers, python headers, here
include_dirs = ["/opt/local/include",
    "/opt/local/Library/Frameworks/Python.framework/Versions/3.4/include/python3.4m/",
    "/opt/local/Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/site-packages/numpy/core/include",
    "."]
libraries=["boost_python3-mt"]
library_dirs=['/opt/local/lib', "/opt/local/Library/Frameworks/Python.framework/Versions/3.4/lib/python3.4/"]

files = ["example.cc"]

setup(name="boostexample",
    ext_modules=[
                Extension("boostexample",files,
                library_dirs=library_dirs,
                libraries=libraries,
                include_dirs=include_dirs,
                depends=[]),
                ]
     )
