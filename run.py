import boostexample
import numpy

a = 10*numpy.random.rand(7);
a = a.astype(numpy.int32);
print("Randomly generated numpy.int32 array is {}".format(a));
first = boostexample.getFirst(a);
print("First element returned was {}".format(first));
print("Done!");
