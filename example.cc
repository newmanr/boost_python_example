//Mini boost python test!
#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
#include <iostream>
#include <python.h>
#include "numpy/noprefix.h"
using namespace boost::python;

//Print the first integer from a numpy array of 32 bit integers.  No type safety.
int getFirst(boost::python::numeric::array data)
    {
    int* arr = (int*) PyArray_DATA((PyArrayObject*)data.ptr()); //Embarassingly bad cast.
    std::cout << "Hello from C++ land!  First integer is " << arr[0] <<", maybe."<< std::endl;
    return arr[0];
    }

BOOST_PYTHON_MODULE(boostexample) {
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    def("getFirst", &getFirst);
    }
